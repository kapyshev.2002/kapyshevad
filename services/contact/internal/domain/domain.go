package domain

import "fmt"

type Contact struct {
	ID          int
	FirstName   string
	LastName    string
	PhoneNumber string
}

type Group struct {
	ID   int
	Name string
}

func (c *Contact) FullName() string {
	return fmt.Sprintf("%s %s", c.FirstName, c.LastName)
}
