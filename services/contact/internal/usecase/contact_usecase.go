package usecase

import (
	"architecture_go/services/contact/internal/repository"
	"architecture_go/services/contact/models"
)

type ContactUseCase struct {
	contactRepo repository.ContactRepository
}

func NewContactUseCase(contactRepo repository.ContactRepository) *ContactUseCase {
	return &ContactUseCase{
		contactRepo: contactRepo,
	}
}

func (uc *ContactUseCase) CreateContact(contact *models.Contact) error {
	return nil
}

func (uc *ContactUseCase) GetContactByID(id int) (*models.Contact, error) {
	return nil, nil
}

func (uc *ContactUseCase) UpdateContact(contact *models.Contact) error {
	return nil
}

func (uc *ContactUseCase) DeleteContact(id int) error {
	return nil
}

func (uc *ContactUseCase) CreateGroup(group *models.Group) error {
	return nil
}

func (uc *ContactUseCase) GetGroupByID(id int) (*models.Group, error) {
	return nil, nil
}

func (uc *ContactUseCase) CreateAndAddContactToGroup(contact *models.Contact, groupID int) error {
	return nil

}
