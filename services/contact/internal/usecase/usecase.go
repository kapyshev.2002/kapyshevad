package usecase

type ContactUseCase interface {
	CreateContact(contact *Contact) error
	GetContactByID(id int) (*Contact, error)
	UpdateContact(contact *Contact) error
	DeleteContact(id int) error

	CreateGroup(group *Group) error

	GetGroupByID(id int) (*Group, error)

	CreateAndAddContactToGroup(contact *Contact, groupID int) error
}
