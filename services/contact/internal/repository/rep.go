package repository

type ContactRepository interface {
	CreateContact(contact *models.Contact) error
	GetContactByID(id int) (*models.Contact, error)
	UpdateContact(contact *models.Contact) error
	DeleteContact(id int) error

	CreateGroup(group *models.Group) error
	GetGroupByID(id int) (*models.Group, error)
	AddContactToGroup(contactID int, groupID int) error
}
