package internal

import "architecture_go/services/contact/internal/repository"

func NewContactRepository() repository.ContactRepository {
	return &repository.YourContactRepository{}
}

func NewContactUseCase(contactRepo repository.ContactRepository) usecase.ContactUseCase {
	return &usecase.YourContactUseCase{
		ContactRepo: contactRepo,
	}
}
func NewContactDelivery(contactUseCase usecase.ContactUseCase) delivery.ContactDelivery {
	return &delivery.YourContactDelivery{
		ContactUseCase: contactUseCase,
	}
}
