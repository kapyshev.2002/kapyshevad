package contact

import (
	"log"
	"net/http"

	"architecture_go/services/contact/internal"
)

func Run() {
	contactRepo := internal.NewContactRepository()

	contactUseCase := internal.NewContactUseCase(contactRepo)

	contactDelivery := internal.NewContactDelivery(contactUseCase)

	router := contactDelivery.InitRoutes()

	log.Fatal(http.ListenAndServe(":8080", router))
}

func main() {
	Run()
}
