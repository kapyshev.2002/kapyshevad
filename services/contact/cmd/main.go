package main

import (
	"architecture_go/pkg/store/postgres"
	"log"
)

func main() {
	host := "localhost"
	port := "5432"
	user := "postgres"
	password := "1234"
	dbname := "postgres"

	db, err := postgres.Connect(host, port, user, password, dbname)
	if err != nil {
		log.Fatalf("Ошибка подключения к БД: %v", err)
	}
	defer db.Close()

}
